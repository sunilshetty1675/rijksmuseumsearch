package com.rijksmuseum.search

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.rijksmuseum.core.network.api.ApiService
import com.rijksmuseum.core.repo.CollectionRepository
import com.rijksmuseum.core.utils.LocaleUtil
import com.rijksmuseum.search.ui.collections.CollectionsFragment
import com.rijksmuseum.search.ui.collections.CollectionsViewModel
import kotlinx.coroutines.runBlocking
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.doAnswer
import org.mockito.MockitoAnnotations
import kotlin.test.BeforeTest
import kotlin.test.Test


@RunWith(AndroidJUnit4::class)
class CollectionListErrorState : AutoCloseKoinTest() {
    private lateinit var scenario: FragmentScenario<CollectionsFragment>

    @Mock
    private lateinit var service: ApiService

    @BeforeTest
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        stopKoin()
        startKoin {
            modules(
                module {
                    viewModel { CollectionsViewModel(get()) }
                    single { CollectionRepository(service, get()) }
                    factory { LocaleUtil() }
                }
            )
        }
        scenario = launchFragmentInContainer(themeResId = R.style.NoToolbarTheme)
    }

    @Test
    fun testErrorState(): Unit = runBlocking {
        doAnswer { throw Exception() }.`when`(service)
            .collection(anyString(), anyString(), anyInt(), anyInt())

        scenario.moveToState(newState = Lifecycle.State.STARTED)
        onView(withText(R.string.generic_error_state))
            .check(
                ViewAssertions.matches(
                    withEffectiveVisibility(
                        Visibility.VISIBLE
                    )
                )
            )
        onView(withText(R.string.retry))
            .check(
                ViewAssertions.matches(
                    withEffectiveVisibility(
                        Visibility.VISIBLE
                    )
                )
            )
    }
}