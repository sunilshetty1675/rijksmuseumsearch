package com.rijksmuseum.search

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.paging.PagingData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.core.network.response.Links
import com.rijksmuseum.core.repo.CollectionRepository
import com.rijksmuseum.search.ui.collections.CollectionsFragment
import com.rijksmuseum.search.ui.collections.CollectionsViewModel
import kotlinx.coroutines.flow.flowOf
import org.junit.runner.RunWith
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import kotlin.test.BeforeTest
import kotlin.test.Test


@RunWith(AndroidJUnit4::class)
class CollectionListTest : AutoCloseKoinTest() {
    private lateinit var scenario: FragmentScenario<CollectionsFragment>

    @Mock
    private lateinit var repository: CollectionRepository

    @BeforeTest
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        stopKoin()
        startKoin {
            modules(
                module {
                    viewModel { CollectionsViewModel(repository) }
                }
            )
        }
        scenario = launchFragmentInContainer(themeResId = R.style.NoToolbarTheme)
    }

    @Test
    fun testEmptyState() {
        `when`(repository.collection("")).thenReturn(
            flowOf(
                PagingData.from(
                    emptyList()
                )
            )
        )

        scenario.moveToState(newState = Lifecycle.State.STARTED)
        scenario.onFragment {}
        onView(withId(R.id.empty_text)).check(
            ViewAssertions.matches(
                withEffectiveVisibility(
                    Visibility.VISIBLE
                )
            )
        )
    }

    @Test
    fun testContent() {
        `when`(repository.collection("")).thenAnswer {
            Thread.sleep(2000)
            flowOf(
                PagingData.from(
                    listOf(
                        ArtObjectsItem(
                            principalOrFirstMaker = "Mr. Best Artist Ever",
                            objectNumber = "",
                            productionPlaces = listOf(),
                            links = Links(),
                            hasImage = false,
                            showImage = false,
                            id = "",
                            title = "Best Art Ever",
                            permitDownload = false
                        )
                    )
                )
            )
        }

        scenario.moveToState(newState = Lifecycle.State.STARTED)

        onView(withId(R.id.list)).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
    }
}