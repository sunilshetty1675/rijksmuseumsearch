package com.rijksmuseum.search

import android.app.Application
import com.rijksmuseum.core.di.networkModule
import com.rijksmuseum.search.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RijksMuseumSearchApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@RijksMuseumSearchApplication)
            modules(
                listOf(
                    appModule,
                    networkModule
                )
            )
        }
    }
}