package com.rijksmuseum.search.ui.collections

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.core.repo.CollectionRepository
import kotlinx.coroutines.flow.Flow

class CollectionsViewModel(
    private val repository: CollectionRepository,
) : ViewModel() {
    private var currentQuery: String = ""

    var currentCollectionResult: Flow<PagingData<ArtObjectsItem>>? = null
        private set

    init {
        fetchCollection(currentQuery)
    }

    fun fetchCollection(
        query: String = "",
        forceFetch: Boolean = false
    ): Flow<PagingData<ArtObjectsItem>> {
        val newQuery = formatQuery(query)

        if (!forceFetch) {
            val lastResult = currentCollectionResult
            if (newQuery == currentQuery && lastResult != null) {
                return lastResult
            }
        }

        this.currentQuery = newQuery
        val collectionResult = repository.collection(newQuery).cachedIn(viewModelScope)
        currentCollectionResult = collectionResult
        return collectionResult
    }

    private fun formatQuery(query: String): String {
        return query.replace("\\s".toRegex(), "+")
    }
}