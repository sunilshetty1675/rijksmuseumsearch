package com.rijksmuseum.search.ui.extensions

import android.text.SpannableString
import android.text.style.UnderlineSpan
import androidx.core.content.ContextCompat.getColor
import com.google.android.material.snackbar.Snackbar
import com.rijksmuseum.search.R

fun Snackbar.error(
    actionLinkText: String? = null,
    onActionLinkClicked: (Snackbar) -> Unit = {}
): Snackbar {
    setBackgroundTint(getColor(view.context, R.color.colorMessageError))
    actionLinkText?.let {
        val actionLinkTextSpan = underline(actionLinkText)
        setAction(actionLinkTextSpan) { onActionLinkClicked(this) }
    }
    return this
}

private fun underline(actionLinkText: String?): SpannableString {
    val actionLinkTextSpan = SpannableString(actionLinkText)
    actionLinkTextSpan.setSpan(UnderlineSpan(), 0, actionLinkTextSpan.length, 0)
    return actionLinkTextSpan
}