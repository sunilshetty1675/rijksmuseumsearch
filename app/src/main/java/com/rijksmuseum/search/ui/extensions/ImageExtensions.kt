package com.rijksmuseum.search.ui.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.rijksmuseum.search.R

fun ImageView.defaultImage() {
    setImageResource(R.drawable.ic_image)
}

fun ImageView.setAspectRatio(
    width: Int?,
    height: Int?
) {
    width.let {
        height?.let {
            (layoutParams as? ConstraintLayout.LayoutParams)?.let {
                val aspectRatio = "$width : $height"
                it.dimensionRatio = aspectRatio
            }
        }
    }
}


fun ImageView.setImage(
    imageUrl: String,
    scaleType: ScaleType = ScaleType.FIT_CENTER,
    fadeIn: Boolean = true,
    onSuccess: (drawable: Drawable) -> Unit = {},
    onFailure: () -> Unit = {}

) {

    if (imageUrl.isEmpty()) {
        onFailure()
        return
    }

    val progressIndicator = CircularProgressDrawable(context).also {
        it.strokeWidth = 5f
        it.centerRadius = 30f
        it.setColorSchemeColors(ContextCompat.getColor(context, R.color.progress_indicator))
        it.start()
    }

    Glide.with(context)
        .load(imageUrl)
        .placeholder(progressIndicator)
        .apply(
            when (scaleType) {
                ScaleType.FIT_CENTER -> RequestOptions.fitCenterTransform()
                ScaleType.CENTER_CROP -> RequestOptions.centerCropTransform()
            }
        )
        .apply {
            if (fadeIn) transition(DrawableTransitionOptions.withCrossFade())
        }
        .addListener(object : RequestListener<Drawable> {

            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ) = true

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                resource?.let { onSuccess(it) }
                return false
            }

        })
        .into(this)
}

enum class ScaleType {
    CENTER_CROP,
    FIT_CENTER
}
