package com.rijksmuseum.search.ui.collections

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.search.databinding.ListItemCollectionsBinding
import com.rijksmuseum.search.ui.extensions.ScaleType
import com.rijksmuseum.search.ui.extensions.defaultImage
import com.rijksmuseum.search.ui.extensions.setAspectRatio
import com.rijksmuseum.search.ui.extensions.setImage

class CollectionsViewHolder(
    private val binding: ListItemCollectionsBinding,
    onItemClicked: (Int) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun create(parent: ViewGroup, onItemClicked: (Int) -> Unit): CollectionsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ListItemCollectionsBinding.inflate(inflater, parent, false)
            return CollectionsViewHolder(binding, onItemClicked)
        }
    }

    init {
        itemView.setOnClickListener {
            onItemClicked(absoluteAdapterPosition)
        }
    }

    fun bind(item: ArtObjectsItem) {
        setTextContent(item)
        setupImage(item)
    }

    private fun setTextContent(item: ArtObjectsItem) {
        binding.apply {
            title.text = item.title
            info.text = item.principalOrFirstMaker
        }
    }

    private fun setupImage(item: ArtObjectsItem) {
        val imageUrl = item.webImage?.url
        if (item.hasImage && item.showImage && imageUrl?.isNotEmpty() == true) {
            setupAspectRatio(item)
            binding.backdrop.setImage(
                imageUrl,
                scaleType = ScaleType.FIT_CENTER,
                onFailure = { binding.backdrop.defaultImage() },
                fadeIn = false
            )
        } else {
            binding.backdrop.defaultImage()
        }
    }

    private fun setupAspectRatio(item: ArtObjectsItem) {
        val width = item.webImage?.width
        val height = item.webImage?.height
        binding.backdrop.setAspectRatio(width, height)
    }
}
