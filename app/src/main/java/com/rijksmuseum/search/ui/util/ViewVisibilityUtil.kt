package com.rijksmuseum.search.ui.util

import android.view.View

object ViewVisibilityUtil {
    fun hide(vararg toHide: View) {
        toHide.forEach {
            if (it.visibility != View.GONE) {
                it.visibility = View.GONE
            }
        }
    }

    fun show(vararg toShow: View) {
        toShow.forEach {
            if (it.visibility != View.VISIBLE) {
                it.visibility = View.VISIBLE
            }
        }
    }
}