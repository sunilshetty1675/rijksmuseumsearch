package com.rijksmuseum.search.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rijksmuseum.search.R

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }
}