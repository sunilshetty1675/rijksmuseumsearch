package com.rijksmuseum.search.ui.extensions

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.rijksmuseum.search.R

fun Fragment.navigate(id: Int, args: Bundle? = null, preNavigation: () -> Unit = {}) {
    val navBuilder = NavOptions.Builder()
    navBuilder.setEnterAnim(R.anim.slide_in_right)
        .setExitAnim(R.anim.slide_out_left)
        .setPopEnterAnim(R.anim.slide_in_left)
        .setPopExitAnim(R.anim.slide_out_right)
    preNavigation()
    findNavController().navigate(id, args, navBuilder.build())
}