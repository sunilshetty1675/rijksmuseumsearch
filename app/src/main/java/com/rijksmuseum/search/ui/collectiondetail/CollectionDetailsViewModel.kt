package com.rijksmuseum.search.ui.collectiondetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rijksmuseum.core.network.response.ArtCollectionDetail
import com.rijksmuseum.core.network.response.Result
import com.rijksmuseum.core.repo.CollectionDetailsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CollectionDetailsViewModel(
    val id: String,
    private val repository: CollectionDetailsRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main
) : ViewModel() {

    val state: StateFlow<Result<ArtCollectionDetail>> get() = _state
    private var _state: MutableStateFlow<Result<ArtCollectionDetail>> =
        MutableStateFlow(Result.Fetching)

    init {
        fetchDetails(id)
    }

    fun fetchDetails(id: String) {
        viewModelScope.launch(dispatcher) {
            repository.collectionDetails(id).collect { result ->
                _state.value = result
            }
        }
    }
}