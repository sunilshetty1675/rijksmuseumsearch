package com.rijksmuseum.search.ui.collections

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.rijksmuseum.core.network.response.ArtObjectsItem

class CollectionsAdapter(val onItemClicked: (ArtObjectsItem) -> Unit) :
    PagingDataAdapter<ArtObjectsItem, CollectionsViewHolder>(ITEM_COMPARATOR) {
    override fun onBindViewHolder(holder: CollectionsViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            holder.bind(repoItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionsViewHolder {
        return CollectionsViewHolder.create(parent) { index ->
            val selectedItem = getItem(index)
            selectedItem?.let { _selectedItem ->
                onItemClicked(_selectedItem)
            }
        }
    }

    fun isEmpty() = itemCount == 0

    companion object {
        private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<ArtObjectsItem>() {
            override fun areItemsTheSame(
                oldItem: ArtObjectsItem,
                newItem: ArtObjectsItem
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ArtObjectsItem,
                newItem: ArtObjectsItem
            ): Boolean =
                oldItem == newItem
        }
    }
}