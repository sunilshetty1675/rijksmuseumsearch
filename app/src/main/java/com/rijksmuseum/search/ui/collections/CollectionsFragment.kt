package com.rijksmuseum.search.ui.collections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.rijksmuseum.core.network.error.NoConnectivityException
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.search.R
import com.rijksmuseum.search.databinding.FragmentCollectionsBinding
import com.rijksmuseum.search.ui.collectiondetail.CollectionDetailsFragment
import com.rijksmuseum.search.ui.extensions.error
import com.rijksmuseum.search.ui.extensions.navigate
import com.rijksmuseum.search.ui.util.ViewVisibilityUtil.hide
import com.rijksmuseum.search.ui.util.ViewVisibilityUtil.show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class CollectionsFragment : Fragment() {

    private val mainCoroutineDispatcher = Dispatchers.Main
    private val viewModel: CollectionsViewModel by viewModel()
    private lateinit var binding: FragmentCollectionsBinding
    private var searchJob: Job? = null

    private val adapter: CollectionsAdapter by lazy {
        CollectionsAdapter(::onItemSelected)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (::binding.isInitialized) {
            return binding.root
        }

        binding = FragmentCollectionsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if ((binding.list.adapter as? CollectionsAdapter)?.isEmpty() == false) {
            return
        }

        binding.list.adapter = adapter

        collectInitialResult()

        collectAdapterLoadState()

        setUpSearchUI()
    }

    private fun setUpSearchUI() {
        with(binding.search) {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String?): Boolean {
                    return if (newText?.isEmpty() == true) {
                        clearFocus()
                        search()
                        true
                    } else false
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    return query?.let {
                        clearFocus()
                        search(query)
                        true
                    } ?: false
                }
            })
        }
    }

    private fun collectAdapterLoadState() {
        lifecycleScope.launch(mainCoroutineDispatcher) {
            adapter.loadStateFlow.collectLatest { state ->
                renderAdapterLoadState(state)
            }
        }

        lifecycleScope.launch(mainCoroutineDispatcher) {
            adapter.loadStateFlow.distinctUntilChangedBy { it.refresh }
                .filter { it.refresh is LoadState.NotLoading }.collect {
                    (binding.list.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
                        0,
                        0
                    )
                }
        }
    }

    private fun renderAdapterLoadState(state: CombinedLoadStates) {
        val progress = binding.progress
        val emptyText = binding.emptyText
        when (state.refresh) {
            is LoadState.NotLoading -> {
                hide(progress)
                if (adapter.itemCount == 0 && state.prepend.endOfPaginationReached) {
                    show(emptyText)
                    emptyText.text = getString(R.string.no_search_results_state)
                }
            }
            is LoadState.Loading -> {
                show(progress)
                hide(emptyText)
            }
            is LoadState.Error -> {
                hide(progress, emptyText)
                val errorState = state.refresh as? LoadState.Error
                errorState?.error?.let { exception ->
                    showError(exception)
                }
            }
        }
    }

    private fun showError(result: Throwable) {
        with(binding) {
            val message = when (result) {
                is NoConnectivityException -> {
                    getString(R.string.no_network_error_state)
                }
                else -> {
                    getString(R.string.generic_error_state)
                }
            }
            Snackbar.make(root, message, Snackbar.LENGTH_INDEFINITE)
                .error(getString(R.string.retry)) {
                    search(query = binding.search.query.toString(), forceFetch = true)
                }
                .setAnchorView(R.id.snackbar_anchor)
                .setAnimationMode(Snackbar.ANIMATION_MODE_FADE)
                .show()
        }
    }

    private fun collectInitialResult() {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch(mainCoroutineDispatcher) {
            setInitialViewState()
            viewModel.currentCollectionResult?.collectLatest { result ->
                adapter.submitData(result)
            }
        }
    }

    private fun setInitialViewState() {
        show(binding.progress)
    }

    private fun search(query: String = "", forceFetch: Boolean = false) {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch(mainCoroutineDispatcher) {
            viewModel.fetchCollection(query, forceFetch).collectLatest { result ->
                adapter.submitData(result)
            }
        }
    }

    private fun onItemSelected(selectedItem: ArtObjectsItem) {
        val args = bundleOf(CollectionDetailsFragment.OBJECT_NUMBER to selectedItem.objectNumber)
        navigate(R.id.detailFragment, args)
    }

}