package com.rijksmuseum.search.ui.collectiondetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.rijksmuseum.core.network.error.NetworkError
import com.rijksmuseum.core.network.response.ArtCollectionDetail
import com.rijksmuseum.core.network.response.ArtObject
import com.rijksmuseum.core.network.response.Result
import com.rijksmuseum.search.R
import com.rijksmuseum.search.databinding.FragmentCollectionDetailsBinding
import com.rijksmuseum.search.ui.extensions.*
import com.rijksmuseum.search.ui.util.ViewVisibilityUtil.hide
import com.rijksmuseum.search.ui.util.ViewVisibilityUtil.show
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CollectionDetailsFragment : Fragment() {

    companion object {
        const val OBJECT_NUMBER = "OBJECT_NUMBER"
    }

    private val viewModel: CollectionDetailsViewModel by viewModel {
        parametersOf(
            arguments?.getString(OBJECT_NUMBER)
                ?: throw IllegalStateException("No OBJECT_ID passed as argument to Fragment")
        )
    }

    private lateinit var binding: FragmentCollectionDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCollectionDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()

        collectDetails()
    }

    private fun setupToolbar() {
        binding.appBar.toolBar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun collectDetails() {
        lifecycleScope.launch(Dispatchers.Main) {
            viewModel.state.collect { result ->
                when (result) {
                    Result.Fetching -> {
                        showLoadingState()
                    }
                    is Result.Value -> {
                        setContent(result)
                    }
                    Result.Empty -> {
                        showEmptyState()
                    }
                    is Result.Error -> {
                        showErrorState(result)
                    }
                }
            }
        }
    }

    private fun showLoadingState() {
        with(binding) {
            hide(backdrop, title, subTitle, overviewTitle, overviewDescription)
            show(progress)
        }
    }

    private fun setContent(result: Result.Value<ArtCollectionDetail>) {
        with(binding) {
            hide(progress)
            with(result.value.artObject) {
                setupImage(this)
                setupTitle(this)
                setupSubTitle(this)
                setupOverviewDescription(this)
            }
        }
    }

    private fun setupImage(item: ArtObject) {
        with(binding.backdrop) {
            show(this)
            val imageUrl = item.webImage?.url
            if (item.hasImage && item.showImage && imageUrl?.isNotEmpty() == true) {
                setAspectRatio(item.webImage?.width, item.webImage?.height)
                setImage(
                    imageUrl,
                    scaleType = ScaleType.FIT_CENTER,
                    onFailure = { defaultImage() },
                    fadeIn = false
                )
            } else {
                defaultImage()
            }
        }
    }

    private fun setupTitle(item: ArtObject) {
        val title = item.title
        val titleView = binding.title
        if (title.isNullOrEmpty()) {
            hide(titleView)
        } else {
            show(titleView)
            titleView.text = title
        }
    }

    private fun setupSubTitle(item: ArtObject) {
        val subTitle = item.subTitle
        val subTitleView = binding.subTitle
        if (subTitle.isNullOrEmpty()) {
            hide(subTitleView)
        } else {
            show(subTitleView)
            subTitleView.text = subTitle
        }
    }

    private fun setupOverviewDescription(item: ArtObject) {
        val description = item.description
        val descriptionView = binding.overviewDescription
        if (description.isNullOrEmpty()) {
            hide(descriptionView)
        } else {
            show(descriptionView)
            descriptionView.text = description
        }
    }

    private fun showEmptyState() {
        with(binding) {
            hide(
                backdrop,
                title,
                subTitle,
                overviewTitle,
                overviewDescription,
                progress
            )
        }
    }

    private fun showErrorState(result: Result.Error) {
        with(binding) {
            hide(progress)
            val message = when (result.errorType) {
                is NetworkError -> {
                    getString(R.string.no_network_error_state)
                }
                else -> {
                    getString(R.string.generic_error_state)
                }
            }
            Snackbar.make(root, message, Snackbar.LENGTH_INDEFINITE)
                .error(getString(R.string.retry)) {
                    viewModel.fetchDetails(viewModel.id)
                }
                .setAnchorView(R.id.snackbar_anchor)
                .setAnimationMode(Snackbar.ANIMATION_MODE_FADE)
                .show()
        }
    }
}