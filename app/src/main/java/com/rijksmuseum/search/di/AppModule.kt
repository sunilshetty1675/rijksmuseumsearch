package com.rijksmuseum.search.di

import com.rijksmuseum.search.ui.collectiondetail.CollectionDetailsViewModel
import com.rijksmuseum.search.ui.collections.CollectionsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { CollectionsViewModel(get()) }
    viewModel { (id:String) -> CollectionDetailsViewModel(id, get()) }
}
