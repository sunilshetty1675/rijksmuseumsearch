# README #

### What is this project about? ###
It is a catalogue of all art contained in the Rijks Musesum in Amsterdam.
App consits of two screens, a primary list which with search and a details screen.  

### Programming language ###
Kotlin

### Patterns ###
MVVM, Repository, Single Activity, Navigation component.

### Libraries ###
Retrofit, OkHttp, Glide

### Tests ###
JUnit, Espresso
