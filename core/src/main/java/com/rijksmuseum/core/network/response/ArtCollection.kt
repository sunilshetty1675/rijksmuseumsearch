package com.rijksmuseum.core.network.response

import com.google.gson.annotations.SerializedName

data class ArtCollection(

    @field:SerializedName("artObjects")
    val artObjects: List<ArtObjectsItem>,

    @field:SerializedName("countFacets")
    val countFacets: CountFacets,

    @field:SerializedName("count")
    val count: Int,

    @field:SerializedName("elapsedMilliseconds")
    val elapsedMilliseconds: Int,

    @field:SerializedName("facets")
    val facets: List<FacetsItem>
)

data class FacetsItem(

    @field:SerializedName("prettyName")
    val prettyName: Int,

    @field:SerializedName("otherTerms")
    val otherTerms: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("facets")
    val facets: List<Facet>,
)

data class Facet(
    @field:SerializedName("value")
    val value: Int,

    @field:SerializedName("key")
    val key: String
)

data class HeaderImage(

    @field:SerializedName("offsetPercentageY")
    val offsetPercentageY: Int,

    @field:SerializedName("offsetPercentageX")
    val offsetPercentageX: Int,

    @field:SerializedName("width")
    val width: Int,

    @field:SerializedName("guid")
    val guid: String,

    @field:SerializedName("url")
    val url: String,

    @field:SerializedName("height")
    val height: Int
)

data class ArtObjectsItem(

    @field:SerializedName("principalOrFirstMaker")
    val principalOrFirstMaker: String,

    @field:SerializedName("webImage")
    val webImage: WebImage? = null,

    @field:SerializedName("headerImage")
    val headerImage: HeaderImage? = null,

    @field:SerializedName("objectNumber")
    val objectNumber: String,

    @field:SerializedName("productionPlaces")
    val productionPlaces: List<String>,

    @field:SerializedName("links")
    val links: Links,

    @field:SerializedName("hasImage")
    val hasImage: Boolean,

    @field:SerializedName("showImage")
    val showImage: Boolean,

    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("title")
    val title: String,

    @field:SerializedName("longTitle")
    val longTitle: String? = null,

    @field:SerializedName("permitDownload")
    val permitDownload: Boolean
)

data class WebImage(

    @field:SerializedName("offsetPercentageY")
    val offsetPercentageY: Int,

    @field:SerializedName("offsetPercentageX")
    val offsetPercentageX: Int,

    @field:SerializedName("width")
    val width: Int,

    @field:SerializedName("guid")
    val guid: String,

    @field:SerializedName("url")
    val url: String,

    @field:SerializedName("height")
    val height: Int
)

data class Links(

    @field:SerializedName("web")
    val web: String? = null,

    @field:SerializedName("self")
    val self: String? = null,

    @field:SerializedName("search")
    val search: String? = null
)

data class CountFacets(

    @field:SerializedName("ondisplay")
    val ondisplay: Int,

    @field:SerializedName("hasimage")
    val hasimage: Int
)
