package com.rijksmuseum.core.network.error

interface ErrorType

data class NetworkError(val type: NetworkErrorType): ErrorType

data class GenericError(val error: ErrorResponse? = null): ErrorType

enum class NetworkErrorType {
    NO_INTERNET,
    TIMEOUT
}