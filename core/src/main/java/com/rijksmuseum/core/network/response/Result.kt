package com.rijksmuseum.core.network.response

import com.rijksmuseum.core.network.error.ErrorType

sealed class Result<out T> {
    object Fetching: Result<Nothing>()
    class Value<out T>(val value: T) : Result<T>()
    class Error(val errorType: ErrorType): Result<Nothing>()
    object Empty : Result<Nothing>()
}
