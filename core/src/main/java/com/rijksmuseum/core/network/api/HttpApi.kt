package com.rijksmuseum.core.network.api

import com.rijksmuseum.core.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class HttpApi {

    fun <Service> service(
        baseUrl: String,
        apiService: Class<Service>,
        vararg interceptors: Interceptor?,
        timeOutSeconds: Long = 30
    ): Service {
        val builder = OkHttpClient.Builder()

        interceptors.forEach { interceptor ->
            interceptor?.let {
                builder.addInterceptor(interceptor)
            }
        }

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }

        builder.readTimeout(timeOutSeconds, TimeUnit.SECONDS)

        builder.connectTimeout(timeOutSeconds, TimeUnit.SECONDS)
        builder.followRedirects(true)

        val client = builder.build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(apiService)
    }
}
