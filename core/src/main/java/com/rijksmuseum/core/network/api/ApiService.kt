package com.rijksmuseum.core.network.api

import com.rijksmuseum.core.network.response.ArtCollection
import com.rijksmuseum.core.network.response.ArtCollectionDetail
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET(Endpoints.COLLECTION)
    suspend fun collection(
        @Path("culture") locale: String,
        @Query("q") query: String,
        @Query("p") page: Int,
        @Query("ps") pageSize: Int,
    ): Response<ArtCollection>

    @GET(Endpoints.COLLECTION_DETAILS)
    suspend fun collectionDetails(
        @Path("culture") locale: String,
        @Path("object-number") id: String
    ): Response<ArtCollectionDetail>
}