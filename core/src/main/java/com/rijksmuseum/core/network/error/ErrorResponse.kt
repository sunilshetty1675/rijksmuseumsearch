package com.rijksmuseum.core.network.error

import com.google.gson.Gson
import retrofit2.Response

abstract class ErrorResponse {
    var httpResponseCode: String = ""
}

fun <T, R : ErrorResponse> convertErrorBody(
    response: Response<T>,
    destinationClass: Class<R>
): ErrorResponse? {
    return try {
        val errorBody = response.errorBody()?.string()
        errorBody?.let { errorString ->
            val errorResponse = Gson().fromJson(errorString, destinationClass)
            errorResponse.httpResponseCode = response.code().toString()
            errorResponse
        }
    } catch (exception: Exception) {
        null
    }
}










