package com.rijksmuseum.core.network.api

object Endpoints {
    const val API_HOST = "https://www.rijksmuseum.nl"

    const val COLLECTION = "/api/{culture}/collection"
    const val COLLECTION_DETAILS = "/api/{culture}/collection/{object-number}"
}
