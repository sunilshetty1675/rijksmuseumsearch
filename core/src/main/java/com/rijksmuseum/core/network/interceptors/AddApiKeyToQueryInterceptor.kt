package com.rijksmuseum.core.network.interceptors

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AddApiKeyToQueryInterceptor(private val apiKey: String) : Interceptor {
    companion object {
        private const val API_KEY = "key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url()

        val builder = originalHttpUrl.newBuilder()
        val url = builder
            .addQueryParameter(API_KEY, apiKey)
            .build()

        val requestBuilder: Request.Builder = original.newBuilder()
            .url(url)

        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}