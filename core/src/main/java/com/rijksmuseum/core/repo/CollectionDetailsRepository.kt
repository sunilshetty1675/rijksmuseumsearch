package com.rijksmuseum.core.repo

import com.rijksmuseum.core.network.api.ApiService
import com.rijksmuseum.core.network.error.GenericError
import com.rijksmuseum.core.network.error.NetworkError
import com.rijksmuseum.core.network.error.NetworkErrorType
import com.rijksmuseum.core.network.error.NoConnectivityException
import com.rijksmuseum.core.network.response.Result
import com.rijksmuseum.core.utils.LocaleUtil
import com.rijksmuseum.test.OpenForTesting
import kotlinx.coroutines.flow.flow
import java.net.SocketTimeoutException

@OpenForTesting
class CollectionDetailsRepository(
    private val service: ApiService,
    private val localeUtil: LocaleUtil
) {

    suspend fun collectionDetails(id: String) = flow {
        try {
            emit(Result.Fetching)
            val details = service.collectionDetails(localeUtil.getDefaultLanguageCode(), id)

            if (details.isSuccessful) {
                details.body()?.let { _details ->
                    emit(Result.Value(_details))
                } ?: emit(Result.Empty)
            } else {
                emit(Result.Error(GenericError()))
            }
        } catch (e: Exception) {
            when (e) {
                is NoConnectivityException -> {
                    emit(Result.Error(NetworkError(NetworkErrorType.NO_INTERNET)))
                }
                is SocketTimeoutException -> {
                    emit(Result.Error(NetworkError(NetworkErrorType.TIMEOUT)))
                }
                else -> {
                    emit(Result.Error(GenericError()))
                }
            }
        }
    }
}