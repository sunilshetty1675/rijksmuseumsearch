package com.rijksmuseum.core.repo

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.rijksmuseum.core.network.api.ApiService
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.core.utils.LocaleUtil

class CollectionsPagingSource(
    private val apiService: ApiService,
    private val localeUtil: LocaleUtil,
    private val searchQuery: String
) : PagingSource<Int, ArtObjectsItem>() {
    companion object {
        const val DEFAULT_PAGE_SIZE = 12
        const val STARTING_PAGE = 1
    }

    override fun getRefreshKey(state: PagingState<Int, ArtObjectsItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArtObjectsItem> {
        val position = params.key ?: STARTING_PAGE
        return try {
            val collection = apiService.collection(
                localeUtil.getDefaultLanguageCode(),
                searchQuery,
                position,
                DEFAULT_PAGE_SIZE
            )
            if (collection.isSuccessful) {
                collection.body()?.let { _collection ->
                    val artObjects = _collection.artObjects
                    val nextKey = if (artObjects.isEmpty()) {
                        null
                    } else {
                        position + (params.loadSize / DEFAULT_PAGE_SIZE)
                    }
                    LoadResult.Page(
                        data = artObjects,
                        prevKey = null, // Only paging forward.
                        nextKey = nextKey
                    )
                } ?: run {
                    LoadResult.Page(
                        data = listOf(),
                        prevKey = null, // Only paging forward.
                        nextKey = null
                    )
                }
            } else {
                LoadResult.Error(Throwable())
            }
        } catch (e: Exception) {
                LoadResult.Error(e)
        }
    }
}