package com.rijksmuseum.core.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.rijksmuseum.core.network.api.ApiService
import com.rijksmuseum.core.network.response.ArtObjectsItem
import com.rijksmuseum.core.utils.LocaleUtil
import com.rijksmuseum.test.OpenForTesting
import kotlinx.coroutines.flow.Flow

@OpenForTesting
class CollectionRepository(private val service: ApiService, private val localeUtil: LocaleUtil) {

    fun collection(query: String): Flow<PagingData<ArtObjectsItem>> {
        val config =
            PagingConfig(CollectionsPagingSource.DEFAULT_PAGE_SIZE, enablePlaceholders = false, initialLoadSize = CollectionsPagingSource.DEFAULT_PAGE_SIZE)
        val pagingSourceFactory: () -> PagingSource<Int, ArtObjectsItem> =
            { CollectionsPagingSource(service, localeUtil, query) }
        return Pager(
            config,
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }
}