package com.rijksmuseum.core.di

import com.rijksmuseum.core.BuildConfig
import com.rijksmuseum.core.network.api.ApiService
import com.rijksmuseum.core.network.api.Endpoints
import com.rijksmuseum.core.network.api.HttpApi
import com.rijksmuseum.core.network.interceptors.AddApiKeyToQueryInterceptor
import com.rijksmuseum.core.network.interceptors.NetworkConnectionInterceptor
import com.rijksmuseum.core.repo.CollectionDetailsRepository
import com.rijksmuseum.core.repo.CollectionRepository
import com.rijksmuseum.core.utils.LocaleUtil
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.lang.ref.WeakReference

val networkModule = module {
    single {
        NetworkConnectionInterceptor(WeakReference(androidContext()))
    }

    single {
        AddApiKeyToQueryInterceptor(BuildConfig.API_KEY)
    }

    single {
        CollectionRepository(get(), get())
    }

    single {
        CollectionDetailsRepository(get(), get())
    }

    factory {
        LocaleUtil()
    }

    single {
        val networkConnectionInterceptor: NetworkConnectionInterceptor = get()
        val apiKeyToQueryInterceptor: AddApiKeyToQueryInterceptor = get()
        HttpApi().service(
            Endpoints.API_HOST,
            ApiService::class.java,
            networkConnectionInterceptor,
            apiKeyToQueryInterceptor
        )
    }
}