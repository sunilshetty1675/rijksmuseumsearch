package com.rijksmuseum.core.utils

import java.util.*

class LocaleUtil {
    fun getDefaultLanguageCode(): String {
       return  Locale.getDefault().language
    }
}