package com.rijksmuseum.test

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting