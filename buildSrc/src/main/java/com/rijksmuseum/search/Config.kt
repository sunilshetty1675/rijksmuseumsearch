package com.rijksmuseum.search

object Config {
    object Plugins {
        const val gradle = Versions.gradle
        const val kotlin = Versions.Kotlin.core
    }
}