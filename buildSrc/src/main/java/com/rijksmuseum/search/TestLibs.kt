package com.rijksmuseum.search

object TestLibs {

    object Espresso {
        const val core = "androidx.test.espresso:espresso-core:${TestVersions.Espresso.core}"
        const val web = "androidx.test.espresso:espresso-web:${TestVersions.Espresso.core}"
        const val uiautomator = "androidx.test.uiautomator:uiautomator:${TestVersions.Espresso.uiautomator}"
        const val runner = "androidx.test:runner:${TestVersions.Espresso.runner}"
        const val rules = "androidx.test:rules:${TestVersions.Espresso.rules}"
    }

    object Dexmaker {
        const val mockito = "com.linkedin.dexmaker:dexmaker-mockito:2.28.1"
    }

    const val junit = "junit:junit:${TestVersions.junit}"
    const val androidJunit = "androidx.test.ext:junit:${TestVersions.androidJunit}"

    const val koin_junit_test = "io.insert-koin:koin-test-junit4:${TestVersions.koin}"
    const val koin_test = "io.insert-koin:koin-core:${TestVersions.koin}"

    const val fragment_test = "androidx.fragment:fragment-testing:${TestVersions.fragment}"
}