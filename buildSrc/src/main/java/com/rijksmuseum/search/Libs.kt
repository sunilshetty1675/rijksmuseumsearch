package com.rijksmuseum.search

object Libs {

    object Kotlin {
        const val core = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.Kotlin.core}"
        const val coroutines_android =
            "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.Kotlin.kotlinx_coroutines}"
    }

    object AndroidX {
        const val core = "androidx.core:core-ktx:${Versions.AndroidX.core}"
        const val activity = "androidx.activity:activity-ktx:${Versions.AndroidX.activity}"
        const val appcompat = "androidx.appcompat:appcompat:${Versions.AndroidX.appcompat}"
        const val material = "com.google.android.material:material:${Versions.AndroidX.material}"
        const val recyclerview =
            "androidx.recyclerview:recyclerview:${Versions.AndroidX.recyclerview}"
        const val cardview = "androidx.cardview:cardview:${Versions.AndroidX.cardview}"
        const val media = "androidx.media:media:${Versions.AndroidX.media}"
        const val legacy = "androidx.fragment:fragment:${Versions.AndroidX.fragment}"
        const val multidex = "androidx.multidex:multidex:${Versions.AndroidX.multidex}"
        const val annotationss = "androidx.annotation:annotation:${Versions.AndroidX.annotation}"
        const val constraintlayout =
            "androidx.constraintlayout:constraintlayout:${Versions.AndroidX.constraintlayout}"
        const val lifecycle_viewModel =
            "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.AndroidX.lifecycle}"
        const val lifecycle_runtime =
            "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.AndroidX.lifecycle}"
        const val lifecycle_livedata =
            "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.AndroidX.lifecycle}"
        const val fragmentktx = "androidx.fragment:fragment-ktx:${Versions.AndroidX.fragmentKtx}"
        const val swipe_refresh_layout =
            "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.AndroidX.swipeRefresh}"
        const val navigation_fragment =
            "androidx.navigation:navigation-fragment-ktx:${Versions.AndroidX.navigation}"
        const val navigation_ui =
            "androidx.navigation:navigation-ui-ktx:${Versions.AndroidX.navigation}"
        const val sharedPreferences =
            "androidx.preference:preference-ktx:${Versions.AndroidX.preference_version}"
        const val paging = "androidx.paging:paging-runtime:${Versions.AndroidX.paging_version}"
    }

    object Lifecycle {
        const val extensions =
            "androidx.lifecycle:lifecycle-extensions:${Versions.Lifecycle.extensions}"
        const val reactivestreams =
            "androidx.lifecycle:lifecycle-reactivestreams:${Versions.Lifecycle.reactivestreams}"
    }

    object Koin {
        const val core = "io.insert-koin:koin-core:${Versions.koin}"
        const val android = "io.insert-koin:koin-android:${Versions.koin}"
    }

    object Retrofit {
        const val core = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
        const val converter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
        const val adapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
        const val moshi_converter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    }

    object OkHttp {
        const val core = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
        const val logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    }

    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val javax_annotation =
        "javax.annotation:javax.annotation-api:${Versions.javax_annotation}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
}