package com.rijksmuseum.search

object Versions {

    object SDK {
        const val minSdk = 19
        const val targetSdk = 30
        const val compileSdk = 30
        const val buildTools = "30.0.3"
    }

    object AndroidX {
        const val core = "1.1.0"
        const val activity = "1.0.0"
        const val appcompat = "1.1.0"
        const val material = "1.3.0-alpha01"
        const val recyclerview = "1.0.0"
        const val cardview = "1.0.0"
        const val media = "1.0.0"
        const val fragment = "1.1.0"
        const val navigation = "2.3.5"
        const val multidex = "2.0.0"
        const val annotation = "1.1.0"
        const val constraintlayout = "1.1.3"
        const val lifecycle = "2.4.0-alpha02"
        const val fragmentKtx = "1.3.0"
        const val swipeRefresh = "1.1.0"
        const val preference_version = "1.1.1"
        const val paging_version = "3.0.0"
    }

    object Lifecycle {
        const val extensions = "2.1.0"
        const val reactivestreams = "2.1.0"
    }

    object Kotlin {
        const val core = "1.4.30"
        const val kotlinx_coroutines = "1.4.2"
        const val jvm_target = "1.8"
    }

    const val gradle = "4.1.3"
    const val koin = "3.1.2"
    const val retrofit = "2.6.2"
    const val okhttp = "3.9.1"
    const val gson = "2.8.5"
    const val javax_annotation = "1.2"
    const val glide = "4.12.0"
}