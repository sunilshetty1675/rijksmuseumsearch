package com.rijksmuseum.search

object TestVersions {
    object Espresso {
        const val core = "3.3.0"
        const val uiautomator = "2.2.0"
        const val runner = "1.1.0"
        const val rules = "1.1.0"
    }

    const val junit = "4.+"
    const val androidJunit = "1.1.2"
    const val koin = "3.1.2"
    const val fragment = "1.3.3"
}